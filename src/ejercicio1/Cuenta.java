/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio1;

/**
 *
 * @author Core i3
 */
public class Cuenta {
    private int Numero;
    private int saldoMax;
    private int giroMax;
    private String tipoCuenta;
    final Cliente cliente;
    
    
    public Cuenta(int numero, String tipo,Cliente cliente){
        Numero=numero;
        tipoCuenta=tipo;
        this.cliente=cliente;
        asignarMontos(tipo);
    }
    
    private void asignarMontos(String tipo){
        if(tipo.equals('C')){
            saldoMax=1000;
            giroMax=100;
        }
        if(tipo.equals('B')){
            saldoMax=2000;
            giroMax=200;
        }
        if(tipo.equals('A')){
            saldoMax=3000;
            giroMax=300;
        }
    }
    
    //Metodos get
    public int getNumero(){
        return Numero;
    }
    public String getTipo(){
        return tipoCuenta;
    }
    
    public int getSaldo(){
        return saldoMax;
    }
    public int getGiro(){
        return giroMax;
    }
    public Cliente getCliente(){
        return cliente;
    }
    
    //Metodos set
    public void setNumero(int numero){
        Numero=numero;
    }
    
    
}
