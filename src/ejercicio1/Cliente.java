/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio1;

/**
 *
 * @author Core i3
 */
public class Cliente {
    private String Nombre;
    private String Rut;
    private int Ingresos;
    private String tipoCliente;
    
    public Cliente(String nombre, String rut, int ingresos){
        Nombre=nombre;
        Rut=rut;
        Ingresos=ingresos;
        setTipo(ingresos);
    }
    
    
    
    
    //Metodos get
    public String getNombre(){
        return Nombre;
        
        
        
    }     
    
    public String getRut(){
        return Rut;
    }  
    public int getIngresos(){
        return Ingresos;
    }
    public String getTipo(){
        return tipoCliente;
    }
    
   //Metodos set
    public void setNombre(String nombre){
        Nombre=nombre;
    }
    public void setRut(String rut){
        Rut=rut;
    }
    
   //Al modificar los ingresos del cliente se debe reevaluar a tipo de pertenece 
   public void setIngresos(int ingresos){
       Ingresos=ingresos;
       setTipo(ingresos);
   }
   
   //Se asigna el tipo de cliente segun sus ingresos
   private void setTipo(int ingresos){
       
       if(ingresos<=500&&ingresos>100){
            tipoCliente="C";
        }
        if(ingresos<=1000&&ingresos>500){
            tipoCliente="B";
        }
        if(ingresos>1000){
            tipoCliente="A";
        }
   }
    
    
}
