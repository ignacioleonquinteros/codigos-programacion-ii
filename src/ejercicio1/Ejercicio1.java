/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ejercicio1;

/**
 *
 * @author Core i3
 */
public class Ejercicio1 {

    
    public static Cuenta asignarCuenta(Cliente C, int numero){
        Cuenta cuenta=new Cuenta(0," ",C);
        if(C.getTipo().equals("C")){
            cuenta = new Cuenta(numero,"C",C);
            return cuenta;
        }
        if(C.getTipo().equals("B")){
            cuenta = new Cuenta(numero,"B",C);
            return cuenta;
        }
        if(C.getTipo().equals("A")){
            cuenta = new Cuenta(numero,"A",C);
            return cuenta;
        }
        
        return cuenta;
    }
    
    
    public static void main(String[] args) {
        
        Cliente cliente1=new Cliente("Pedro","12345678-0",1500);
        Cliente cliente2=new Cliente("Juan","87654321-0",800);
        Cliente cliente3=new Cliente("Diego","11111111-1",200);
        
        Cuenta cuenta1= asignarCuenta(cliente1,382392);
        Cuenta cuenta2= asignarCuenta(cliente2,382965);
        Cuenta cuenta3= asignarCuenta(cliente3,765392);
        
        
        System.out.println("Datos cuenta 1:");
        System.out.println(cuenta1.getCliente().getNombre());
        System.out.println(cuenta1.getTipo());
        
        
    }
    
}
